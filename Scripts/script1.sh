#!/bin/bash

latest_commit_local=$(git rev-parse main)

git fetch origin main
latest_commit_remote=$(git rev-parse origin/main)

if [ "$latest_commit_local" == "$latest_commit_remote" ]; then
  echo "No differences found between main and origin/main."
  exit 0
fi

new_tag=$(git rev-parse --short origin/main)

echo -n "Enter your dockerhub username: "
read  docker_username
echo -n "Enter your dockerhub password: "
read -s docker_password

if [[ ! $(echo "$docker_password" | docker login --username "$docker_username" --password-stdin) ]]; then
  exit 1
fi

docker build -t redonbasha/react-todo-app:$new_tag react-todo-app/
docker push redonbasha/react-todo-app:$new_tag

helm upgrade assignment ./react-todo-app-chart --set deployment.containers.imageTag=$new_tag
helm list -aA

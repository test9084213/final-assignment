#!/bin/bash

latest_commit_local_main=$(git rev-parse main)
latest_commit_local_develop=$(git rev-parse develop)

if (git merge-base --is-ancestor $latest_commit_local_develop $latest_commit_local_main ); then
  echo "New merge in main branch."

  last_number=$(cat react-todo-app/Release.txt | awk 'NR==1{print $3}' | awk -F. '{print $NF}')
  new_number=$(($last_number+1))
  new_release_number="1.0.$new_number"
  sudo sed -i "1s/.*/App_version : $new_release_number/" react-todo-app/Release.txt

  new_commit=$(git rev-parse --short main)
  sudo sed -i "2s/.*/Latest_commit : $new_commit/" react-todo-app/Release.txt

  git add react-todo-app/Release.txt
  git commit -m "New release number: $new_release_number"
  sudo git push origin main

else
    echo "No new merge in main branch."
fi